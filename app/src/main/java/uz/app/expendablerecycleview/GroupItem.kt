package uz.app.expendablerecycleview

class GroupItem(val header: Header, var itemList: List<Item>) {
    class Header(val textHeader: String?)
    class Item(val textItem: String?)
}