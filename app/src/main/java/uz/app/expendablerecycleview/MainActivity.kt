package uz.app.expendablerecycleview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.app.expendablerecycleview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.frame_layout,IntroFragment(this)).commit()
    }
}