package uz.app.expendablerecycleview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate

class RecycleViewAdapter(private val groupItem: GroupItem) :
    RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder>() {
    companion object {
        const val VIEW_HEADER = 0
        const val VIEW_SUB = 1
    }

    var isExpanded = false

    sealed class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        class HeaderViewHolder(itemView: View) : MyViewHolder(itemView)
        val headerTextView = itemView.findViewById<TextView>(R.id.tv_header)
        fun onBind(header: GroupItem.Header, onClickListener: View.OnClickListener) {
            headerTextView.text = header.textHeader
            itemView.setOnClickListener {
                onClickListener.onClick(it)
            }
        }

        class SubItemViewHolder(itemView: View) : MyViewHolder(itemView) {
            private val itemTextView = itemView.findViewById<TextView>(R.id.tv_sub)
            fun onBind(item: GroupItem.Item) {
                itemTextView.text = item.textItem

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_HEADER -> {
                MyViewHolder.HeaderViewHolder(
                    layoutInflater.inflate(
                        R.layout.header_item,
                        parent,
                        false
                    )
                )
            }
            else -> {
                MyViewHolder.SubItemViewHolder(
                    layoutInflater.inflate(
                        R.layout.sub_item,
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return if (isExpanded) {
            groupItem.itemList.size + 1
        } else
            1
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        when (holder) {
            is MyViewHolder.HeaderViewHolder -> holder.onBind(groupItem.header, onHeaderClicked())
            is MyViewHolder.SubItemViewHolder -> holder.onBind(groupItem.itemList[position - 1])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            VIEW_HEADER
        } else
            VIEW_SUB
    }

    fun onHeaderClicked() = View.OnClickListener {
        isExpanded = !isExpanded
        if (isExpanded) {
            notifyItemRangeInserted(1, groupItem.itemList.size)
            notifyItemChanged(0)
        } else {
            notifyItemRangeRemoved(1, groupItem.itemList.size)
            notifyItemChanged(1)
        }
    }
}