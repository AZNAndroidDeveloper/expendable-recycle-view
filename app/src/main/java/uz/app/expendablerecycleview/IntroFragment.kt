package uz.app.expendablerecycleview

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import uz.app.expendablerecycleview.databinding.FragmentIntroBinding


class IntroFragment(var mContext: Context?) : Fragment(R.layout.fragment_intro) {
    init {
     mContext = context
    }
    private lateinit var binding:FragmentIntroBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        val adapterList = arrayListOf<RecycleViewAdapter>()
        val groupList =  createItem(35)
        for (item in groupList){
            val adapter  =RecycleViewAdapter(item)
            adapterList.add(adapter)
        }
        val concatConfig  = ConcatAdapter.Config.Builder().setIsolateViewTypes(false).build()
        val adapter = ConcatAdapter(concatConfig,adapterList)
        binding.recycleView.layoutManager = LinearLayoutManager(activity!!.applicationContext,LinearLayoutManager.VERTICAL,false)
        binding.recycleView.adapter = adapter
    }

    fun createItem(numberOfHeader:Int):List<GroupItem>{
        val groupList = arrayListOf<GroupItem>()
        for (i in 0 until numberOfHeader){
            val txtHeader  = "Title $i"
            val header = GroupItem.Header(txtHeader)
            val itemsList = arrayListOf<GroupItem.Item>()
            val random = (2..8).random()
            for(j in 0 until random){
                val text = "Item $j"
                itemsList.add(GroupItem.Item(text))
            }
            val groupItem = GroupItem(header,itemsList)
           groupList.add(groupItem)
        }
        return groupList

    }
}